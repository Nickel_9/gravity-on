interface IProjects {
  id: string;
  title: string;
  imageSrc?: string;
  link: string;
  linkText?: string;
}

export const PROJECTS: IProjects[] = [
  {
    id: '1',
    title: 'Система управления транспортом',
    imageSrc: '/images/project-one-background.png',
    link: '/'
  },
  {
    id: '2',
    title: 'Система умного дома / IoT',
    link: '/'
  },
  {
    id: '3',
    title: 'Система управления медицинским учреждением',
    link: '/'
  },
  {
    id: '4',
    title: 'Система управления складом (WMS) для замены SAP EWM',
    imageSrc: '/images/project-two-background.png',
    link: '/'
  }
];
