interface IMenuItem {
  id: string;
  href: string;
  title: string;
}

export const MENU_ITEMS: IMenuItem[] = [
  {
    id: 'menu-main-info-link',
    href: '#main-info-anchor',
    title: 'Главная'
  },
  {
    id: 'menu-activity-areas-link',
    href: '#activity-areas-anchor',
    title: 'Направления деятельности'
  },
  {
    id: 'menu-industries-link',
    href: '#industries-anchor',
    title: 'Отрасли'
  },
  {
    id: 'menu-projects-link',
    href: '#projects-anchor',
    title: 'Наши проекты'
  },
  {
    id: 'menu-company-link',
    href: '#company-anchor',
    title: 'Наша компания'
  },
  {
    id: 'menu-work-process-link',
    href: '#work-process-anchor',
    title: 'Подход'
  },
  {
    id: 'menu-founders-link',
    href: '#founders-anchor',
    title: 'Основатели'
  },
  {
    id: 'menu-footer-begin-project-link',
    href: '#footer-anchor',
    title: 'Начать проект'
  }
];
