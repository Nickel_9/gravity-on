interface IFounderInfo {
  id: string;
  fullName: string;
  position: string;
  imageSrc: string;
}

export const FOUNDERS: IFounderInfo[] = [
  {
    id: '1',
    fullName: 'Баранов Алексей Анатольевич',
    position: 'Коммерческий директор',
    imageSrc: '/images/first-founder-profile-photo.png'
  },
  {
    id: '2',
    fullName: 'Распопов Андрей Вячеславович',
    position: 'Генеральный директор, кандидат наук',
    imageSrc: '/images/second-founder-profile-photo.png'
  },
  {
    id: '3',
    fullName: 'Азимбаева Ольга Вячеславовна',
    position: 'Директор по развитию, эксперт в области предпроектного анализа',
    imageSrc: '/images/third-founder-profile-photo.png'
  }
];
