interface IActivityArea {
  id: string;
  title: string;
  description: string;
  imageSrc: string;
  link: string;
  linkText?: string;
}

export const ACTIVITY_AREAS: IActivityArea[] = [
  {
    id: 'outsourcing-projects-anchor',
    title: 'Аутсорсинг IT проектов',
    description: 'Создаем решения с нуля и “под ключ”, начиная с анализа вашей задачи и заканчивая организацией поддержки законченного проекта.',
    imageSrc: '/icons/outsourcing-projects.svg',
    link: '/'
  },
  {
    id: 'consulting-anchor',
    title: 'Консалтинг',
    description: 'Найдем оптимальную стратегию для ваших задач благодаря нашему многолетнему опыту работы по всему миру.',
    imageSrc: '/icons/consulting.svg',
    link: '/'
  },
  {
    id: 'outstaffing-anchor',
    title: 'Аутстафинг',
    description: 'Предоставим кадровые ресурсы, чтобы дополнить вашу команду недостающими компетенциями.',
    imageSrc: '/icons/outstaffing.svg',
    link: '/'
  },
  {
    id: 'software-development-anchor',
    title: 'Разработка ПО',
    description: 'Разработка архитектуры и создание новых решений, отвечающих современным требованиям.',
    imageSrc: '/icons/software-development.svg',
    link: '/'
  },
  {
    id: 'design-anchor',
    title: 'UI & UX дизайн',
    description: 'Визуализация инновационных идей и интефейсов для ваших проектов.',
    imageSrc: '/icons/design.svg',
    link: '/'
  },
  {
    id: 'creating-prototypes-anchor',
    title: 'Создание прототипов',
    description: 'Проработка и реализация наиболее сложных и рискованных аспектов будущего воплощения.',
    imageSrc: '/icons/creating-prototypes.svg',
    link: '/'
  }
];
