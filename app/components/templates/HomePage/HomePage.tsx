import MainInfo from '@modules/MainInfo/MainInfo';
import ActivityAreas from '@modules/ActivityAreas/ActivityAreas';
import Industries from '@modules/Industries/Industries';
import Projects from '@modules/Projects/Projects';
import AboutCompany from '@modules/AboutCompany/AboutCompany';
import WorkProcess from '@modules/WorkProcess/WorkProcess';
import Founders from '@modules/Founders/Founders';

function HomePage() {
  return (
    <main>
      <MainInfo />
      <ActivityAreas />
      <Industries />
      <Projects />
      <AboutCompany />
      <WorkProcess />
      <Founders />
    </main>
  );
}

export default HomePage;
