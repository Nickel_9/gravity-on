import React, { ReactNode } from 'react';
import { Carousel } from 'antd';
import PropTypes from 'prop-types';

interface ICarouselProps {
  children: ReactNode[];
  draggable?: boolean;
  accessibility?: boolean;
  dots?: boolean;
  dotPosition?: 'top' | 'bottom' | 'left' | 'right';
  lazyLoad?: 'ondemand' | 'progressive';
}

function CarouselElement({
  children,
  draggable,
  accessibility,
  dots,
  dotPosition
}: ICarouselProps) {
  return (
    <Carousel
      dots={dots}
      dotPosition={dotPosition}
      draggable={draggable}
      accessibility={accessibility}
    >
      {children.map((child, index) => (
        <span key={index}>{child}</span>
      ))}
    </Carousel>
  );
}

CarouselElement.propTypes = {
  children: PropTypes.node.isRequired,
  draggable: PropTypes.bool,
  accessibility: PropTypes.bool,
  dots: PropTypes.bool,
  dotPosition: PropTypes.string,
  lazyLoad: PropTypes.string
};

CarouselElement.defaultProps = {
  draggable: true,
  accessibility: true,
  dots: true,
  dotPosition: 'bottom',
  lazyLoad: 'ondemand'
};

export default CarouselElement;
