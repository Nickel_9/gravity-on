import { Anchor } from 'antd';
import React from 'react';

import { MENU_ITEMS } from '@constants/menu';
import { ThemeType } from 'app/types/theme-type';

const { Link } = Anchor;

const HEADER_BACKGROUND_SET: Record<string, ThemeType> = {
  '#main-info-anchor': 'light',
  '#activity-areas-anchor': 'dark',
  '#industries-anchor': 'light',
  '#projects-anchor': 'dark',
  '#company-anchor': 'light',
  '#work-process-anchor': 'light',
  '#founders-anchor': 'dark',
  '#footer-anchor': 'light'
};

interface IAnchorsListener {
  // eslint-disable-next-line no-unused-vars
  changeAnchorCallBack: (theme: ThemeType) => void
}

function AnchorsListener({ changeAnchorCallBack }: IAnchorsListener) {
  const onChange = (link: string) => {
    changeAnchorCallBack(HEADER_BACKGROUND_SET[link] || 'default');
  };

  return (
    <Anchor style={{ display: 'none' }} affix={false} onChange={onChange}>
      {MENU_ITEMS.map((item) => (
        <Link key={item.id} href={item.href} title={item.title} />
      ))}
    </Anchor>
  );
}

export default AnchorsListener;
