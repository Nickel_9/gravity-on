import { Anchor } from 'antd';

const { Link } = Anchor;

export function FooterMenu() {
  return (
    <nav className="footer-menu">
      <ul className="nav-columns">
        <li className="nav-column">
          <span className="nav-column-header">Контакты</span>
          <ul className="nav-column-items">
            <li><a id="footer-phone-number-link" href="tel:+79038505552">+7 (903) 850-55-52 </a></li>
            <li><a id="footer-email-link" href="mailto:info@gravity-on.ru" target="_blank" rel="noreferrer">info@gravity-on.ru</a></li>
            <li><a id="footer-telegram-link" href="tg://resolve?domain=HatefulRAV">Связь в Telegram</a></li>
          </ul>
        </li>
        <li className="nav-column">
          <span className="nav-column-header">Направления</span>
          <Anchor affix={false} offsetTop={57} className="nav-column-items">
            <Link className="outsourcing-projects-anchor" href="#outsourcing-projects-anchor" title="Аутсорсинг IT проектов" />
            <Link className="consulting-anchor" href="#consulting-anchor" title="Консалтинг" />
            <Link className="outstaffing-anchor" href="#outstaffing-anchor" title="Аутстаффинг" />
            <Link className="design-anchor" href="#design-anchor" title="UI & UX дизайн" />
            <Link className="software-development-anchor" href="#software-development-anchor" title="Разработка ПО" />
            <Link className="creating-prototypes-anchor" href="#creating-prototypes-anchor" title="Создание прототипов" />
          </Anchor>
        </li>
        <li className="nav-column">
          <span className="nav-column-header">О компании</span>
          <Anchor affix={false} offsetTop={57} className="nav-column-items">
            <Link className="projects-anchor" href="#projects-anchor" title="Наши проекты" />
            <Link className="founders-anchor" href="#founders-anchor" title="Руководство" />
            <Link className="company-anchor" href="#company-anchor" title="Информация" />
          </Anchor>
        </li>
      </ul>
    </nav>
  );
}
