import Image from 'next/image';
import PropTypes from 'prop-types';

import styles from './ProjectCard.module.scss';

interface IProjectCardProps {
  title: string;
  link: string;
  imageSrc?: string;
  linkText?: string;
}

export function ProjectCard({
  title, imageSrc
}: IProjectCardProps) {
  if (imageSrc) {
    return (
      <article className={styles.iconCard}>
        <Image className={styles.background} layout="fill" src={imageSrc} alt="project image" />

        <div className={styles.content}>
          <h5 className={`${styles.title} text-dark`}>{title}</h5>
          {/* <Link href={link}>
            <a
              target="_blank"
              rel="noreferrer"
              aria-label="Open project"
              className="card-button card-button_light"
            >
              {linkText}
            </a>
          </Link> */}
        </div>
      </article>
    );
  }

  return (
    <article className={`${styles.simpleCard} bg-primary`}>
      <h5 className={`${styles.title} text-light`}>{title}</h5>
      {/* <a className="card-button card-button_dark" href={link}>{linkText}</a> */}
    </article>
  );
}

ProjectCard.propTypes = {
  title: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  imageSrc: PropTypes.string,
  linkText: PropTypes.string
};

ProjectCard.defaultProps = {
  linkText: 'Подробнее'
};
