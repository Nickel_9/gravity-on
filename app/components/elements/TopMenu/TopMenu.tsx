import React, { useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { Anchor, Button, Drawer } from 'antd';

import LogoIcon from '@assets/icons/logo-icon.svg';
import TopMenuIcon from '@assets/icons/top-menu-icon.svg';
import CloseMenuIcon from '@assets/icons/close-menu-icon.svg';
import { ScrollManager } from '@helpers/scroll-manager';
import { MENU_ITEMS } from '@constants/menu';

const { Link: AnchorLink } = Anchor;

interface IMenuContentProps {
  hideMenuCallBack: () => void;
}

function MenuContent({ hideMenuCallBack }: IMenuContentProps) {

  const handleClick = () => {
    ScrollManager.setScroll(false);
    hideMenuCallBack();
  };

  return (
    <div className="menu-container limit-width-wrapper ">
      <Anchor affix={false} offsetTop={87} onClick={handleClick} className="menu-content">
        {MENU_ITEMS.map((item) => (
          <AnchorLink key={item.id} href={item.href} title={item.title} className={item.id} />
        ))}
      </Anchor>

      <Image className="menu-background" layout="fill" src="/icons/main-background.svg" alt="activity image" />
    </div>
  );
}

function TopMenu() {

  const [visible, setVisible] = useState(false);

  const showDrawer = () => {
    setVisible(true);
    ScrollManager.setScroll(true);
  };

  const onClose = () => {
    setVisible(false);
    ScrollManager.setScroll(false);
  };

  return (
    <>
      <Drawer
        placement="left"
        closable
        onClose={onClose}
        visible={visible}
        getContainer={false}
        mask={false}
        autoFocus
        extra={(
          <Link href="/">
            <a aria-label="Gravity-on logo">
              <LogoIcon id="menu-logo" width={228} height={64} onClick={onClose} className="menu-logo" />
            </a>
          </Link>
        )}
        closeIcon={<CloseMenuIcon id="menu-close-icon" className="menu-close-button" width={25} height={25} />}
        className="menu"
      >
        <MenuContent hideMenuCallBack={onClose} />
      </Drawer>

      <Button
        htmlType="button"
        type="ghost"
        icon={<TopMenuIcon id="open-menu-icon" width={32} height={30} />}
        onClick={showDrawer}
        aria-label="Open Menu"
        className="menu-button"
      />
    </>
  );
}

export default TopMenu;
