import Image from 'next/image';
import PropTypes from 'prop-types';

import styles from './FounderInfo.module.scss';

interface IFounderInfoProps {
  fullName: string;
  position: string;
  imageSrc: string;
}

export function FounderInfo({
  fullName, position, imageSrc
}: IFounderInfoProps) {
  return (
    <div className={styles.founder}>
      <div className={styles.imageContainer}>
        <Image className={styles.photo} layout="fill" src={imageSrc} alt="founder photo" />
      </div>
      {/* <Avatar className={styles.icon} /> */}

      <div className={styles.info}>
        <h4 className={`${styles.title} text-dark`}>{fullName}</h4>
        <p className={styles.position}>{position}</p>
      </div>
    </div>
  );
}

FounderInfo.propTypes = {
  fullName: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired
};
