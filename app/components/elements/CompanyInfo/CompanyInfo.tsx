import Image from 'next/image';

import NumberElementIcon from '@assets/icons/number-element-icon.svg';

import styles from './CompanyInfo.module.scss';

export function CompanyInfo() {
  return (
    <div className={`${styles.container} bg-primary`}>
      <div className={`${styles.companyInfo}`}>
        <Image className={styles.background} layout="fill" src="/icons/company-info-background.svg" alt="company image" />

        <div className={styles.cards}>
          <div className={styles.point}>
            <div className={styles.countBlock}>
              <NumberElementIcon className={styles.countIcon} height={119} width={119} />
              <span className={`${styles.count} text-blue`}>4</span>
            </div>

            <div className={styles.textInfo}>
              <h3 className={`${styles.title} text-light`}>
                региона
                <br />
                присутствия
              </h3>
              <p className={`${styles.description} text-light`}>
                Москва, Санкт-Петербург,
                Воронеж, Краснодар
              </p>
            </div>
          </div>

          <div className={styles.point}>
            <span className={`${styles.count} text-blue`}>{'> 100'}</span>

            <div className={styles.textInfo}>
              <h3 className={`${styles.title} text-light`}>сотрудников</h3>
              <p className={`${styles.description} text-light`}>
                Мы имеем успешный опыт работы
                с множеством распределенных команд,
                создающих единый продукт
              </p>
            </div>
          </div>

          <div className={styles.point}>
            <span className={`${styles.count} text-blue`}>30 +</span>

            <div className={styles.textInfo}>
              <h3 className={`${styles.title} text-light`}>
                успешных
                <br />
                проектов
              </h3>
              <p className={`${styles.description} text-light`}>
                Лучшие решения для реализации
                ваших проектов
              </p>
            </div>
          </div>

          <div className={styles.point}>
            <span className={`${styles.count} text-blue`}>100 %</span>

            <div className={styles.textInfo}>
              <h3 className={`${styles.title} text-light`}>
                довольных
                <br />
                клиентов
              </h3>
              <p className={`${styles.description} text-light`}>
                Наши клиенты довольны качеством и
                скоростью выполнения задач
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
