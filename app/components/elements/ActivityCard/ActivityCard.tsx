import PropTypes from 'prop-types';
import Image from 'next/image';

import styles from './ActivityCard.module.scss';

interface IActivityCardProps {
  id: string;
  title: string;
  description: string;
  imageSrc: string;
  link: string;
  linkText?: string;
}

export function ActivityCard({
  id, title, description, imageSrc
}: IActivityCardProps) {
  return (
    <article id={id} className={`${styles.card} bg-secondary`}>
      <div className={styles.imageContainer}>
        <Image className={styles.image} layout="fill" src={imageSrc} alt="activity image" />
      </div>

      <div className={styles.content}>
        <div>
          <h5 className={`${styles.title} text-dark`}>{title}</h5>
          <p className={styles.description}>{description}</p>
        </div>
        {/* <Link href={link}>
          <a
            target="_blank"
            rel="noreferrer"
            className="card-button card-button_light"
            aria-label="Open activity"
          >
            {linkText}
          </a>
        </Link> */}
      </div>
    </article>
  );
}

ActivityCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  linkText: PropTypes.string
};

ActivityCard.defaultProps = {
  linkText: 'Подробнее'
};
