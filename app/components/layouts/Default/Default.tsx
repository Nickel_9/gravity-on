import React from 'react';

import Header from '@modules/Header/Header';
import Footer from '@modules/Footer/Footer';

type Props = {
  children: React.ReactNode;
};

function Default({ children }: Props) {
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  );
}

export default Default;
