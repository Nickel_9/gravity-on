import Link from 'next/link';

import TopMenu from '@elements/TopMenu/TopMenu';
import LogoIcon from '@assets/icons/logo-icon.svg';
import LogoLightIcon from '@assets/icons/logo-light-icon.svg';
import LogoDarkIcon from '@assets/icons/logo-dark-icon.svg';
import AnchorsListener from '@ui/AnchorsListener/AnchorsListener';
import { useState } from 'react';
import { ThemeType } from 'app/types/theme-type';

interface ILogoParams {
  themeName: ThemeType;
}

function Logo({ themeName }: ILogoParams) {
  switch (themeName) {
    case 'dark':
      return (
        <Link href="/">
          <a aria-label="Gravity-on logo">
            <LogoLightIcon id="header-mini-logo" width={64} height={30} />
          </a>
        </Link>
      );
    case 'light':
      return (
        <Link href="/">
          <a aria-label="Gravity-on logo">
            <LogoDarkIcon id="header-mini-logo" width={64} height={30} />
          </a>
        </Link>
      );
    default:
      return (
        <Link href="/">
          <a aria-label="Gravity-on logo" className="header-logo">
            <LogoIcon id="header-logo" width={228} height={64} />
          </a>
        </Link>
      );
  }
}

function Header() {
  const [theme, setTheme] = useState<ThemeType>('dark');

  const onChangeAnchor = (themeName: ThemeType) => {
    setTheme(themeName);
  };

  return (
    <header id="header" className={`${theme} header`}>
      <div className="content limit-width-wrapper">
        <Logo themeName={theme} />

        <TopMenu />

        <AnchorsListener changeAnchorCallBack={onChangeAnchor} />
      </div>
    </header>
  );
}

export default Header;
