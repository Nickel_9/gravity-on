import { Button } from 'antd';

import ArrowIcon from '@assets/icons/arrow-icon.svg';
import FooterCirclesIcon from '@assets/icons/footer-circles-icon.svg';
import FooterBackgroundImage from '@assets/icons/footer-background.svg';
import { FooterMenu } from '@elements/FooterMenu/FooterMenu';

function FooterHead() {
  return (
    <div className="footer-head">
      <h3 className="footer-head-title">Начните ваш проект с этой кнопки</h3>

      <ArrowIcon width={240} height={127} className="footer-head-icon" />

      <Button
        id="footer-begin-project-link"
        htmlType="button"
        type="link"
        href="mailto:info@gravity-on.ru"
        target="_blank"
        aria-label="Start Project"
        className="footer-head-button"
      >
        Начать проект
      </Button>
    </div>
  );
}

function Footer() {
  return (
    <footer id="footer-anchor" className="footer">
      <div className="limit-width-wrapper">
        <FooterBackgroundImage className="footer-background" width={1541} height={736} />

        <FooterHead />
        <FooterMenu />

        <FooterCirclesIcon width={361} height={436} className="footer-circle-icon" />
      </div>
    </footer>
  );
}

export default Footer;
