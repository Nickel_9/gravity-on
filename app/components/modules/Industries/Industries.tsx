import IndustriesLargeImage from '@assets/icons/industries-large-icon.svg';
import IndustriesMobileImage from '@assets/icons/industries-mobile-icon.svg';

import styles from './Industries.module.scss';

function Industries() {
  return (
    <section id="industries-anchor" className={`${styles.industries} bg-primary`}>
      <div className="limit-width-wrapper">
        <div className={styles.info}>
          <h2 className={`${styles.title} text-light`}>Отрасли</h2>
          <p className={`${styles.description} text-light`}>
            Нашими специалистами успешно реализованы
            <br />
            проекты в следующих областях:
          </p>
        </div>
        <IndustriesLargeImage className={styles.backgroundLarge} width={1998} height={918} />

        <IndustriesMobileImage className={styles.backgroundMobile} width={1090} height={1173} />
      </div>
    </section>
  );
}

export default Industries;
