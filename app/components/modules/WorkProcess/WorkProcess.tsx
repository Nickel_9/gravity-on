import Image from 'next/image';

import styles from './WorkProcess.module.scss';

function WorkProcess() {
  return (
    <section id="work-process-anchor" className={`${styles.workProcess} bg-secondary`}>
      <div className={`${styles.header} limit-width-wrapper`}>
        <h2 className={`${styles.title} text-dark`}>Гибкость</h2>
        <p className={styles.description}>
          это наш подход, который помогает
          <br />
          развиваться и постоянно совершенствоваться
          <br />
          вместе с вашим продуктом
        </p>
      </div>

      <div className={`${styles.contentContainer} bg-primary`}>
        <div className={`${styles.content} limit-width-wrapper`}>
          <Image className={styles.graphIcon} width={1475} height={655} layout="fill" src="/icons/work-process-graph.svg" alt="work process graph" />

          <Image className={styles.mobileGraphIcon} width={319} height={551} layout="fill" src="/icons/work-process-mobile-graph.svg" alt="work process graph" />
        </div>
      </div>
    </section>
  );
}

export default WorkProcess;
