import { PROJECTS } from '@constants/projects';
import { ProjectCard } from '@elements/ProjectCard/ProjectCard';

import styles from './Projects.module.scss';

function Projects() {
  return (
    <section id="projects-anchor" className={`${styles.projects} bg-secondary`}>
      <div className="limit-width-wrapper">
        <div className={styles.header}>
          <h2 className={`${styles.title} text-dark`}>Наши проекты</h2>
          {/* <Link href="/">
            <a target="_blank" rel="noreferrer" aria-label="Open all projects" className="link">
            смотреть все
          </a>
          </Link> */}
        </div>

        <div className={styles.cards}>
          <div className={styles.content}>
            {PROJECTS.map((item) => (
              <ProjectCard key={item.id} {...item} />
            ))}
          </div>
        </div>
      </div>
    </section>
  );
}

export default Projects;
