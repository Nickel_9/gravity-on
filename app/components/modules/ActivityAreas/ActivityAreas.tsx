import { ACTIVITY_AREAS } from '@constants/activity-areas';
import { ActivityCard } from '@elements/ActivityCard/ActivityCard';

import styles from './ActivityAreas.module.scss';

function ActivityAreas() {
  return (
    <section id="activity-areas-anchor" className={`${styles.areas} bg-secondary`}>
      <div className="limit-width-wrapper">
        <h2 className={`${styles.title} text-dark`}>Направления деятельности</h2>

        <div className={styles.cards}>
          <div className={styles.content}>
            {ACTIVITY_AREAS.map((item) => (
              <ActivityCard key={item.id} {...item} />
            ))}
          </div>
        </div>
      </div>
    </section>
  );
}

export default ActivityAreas;
