import { FOUNDERS } from '@constants/founders';
import { FounderInfo } from '@elements/FounderInfo/FounderInfo';

import styles from './Founders.module.scss';

function Founders() {
  return (
    <section id="founders-anchor" className={`${styles.founders} bg-secondary`}>
      <div className="limit-width-wrapper">
        <h2 className={`${styles.title} text-dark`}>Основатели</h2>

        <div className={styles.cards}>
          {FOUNDERS.map((item) => (
            <FounderInfo key={item.id} {...item} />
          ))}
        </div>
      </div>
    </section>
  );
}

export default Founders;
