import MainComputerIcon from '@assets/icons/main-computer-icon.svg';
import MainBackgroundImage from '@assets/icons/main-background.svg';

import styles from './MainInfo.module.scss';

function FirstSlide() {
  return (
    <div className={`${styles.firstSlide} bg-primary`}>
      <div className={styles.content}>
        <h1 className={`${styles.title} text-light`}>
          Мы притягиваем
          <br />
          технологичные
          <br />
          решения
        </h1>
        <MainBackgroundImage className={styles.background} width={1980} height={730} />

        <MainComputerIcon width={893} height={937} className={styles.icon} />
      </div>
    </div>
  );
}

function MainInfo() {
  return (
    <section id="main-info-anchor" className={styles.mainInfo}>
      <FirstSlide />
      {/* <CarouselElement dots={false} draggable={false}>
        <FirstSlide />
        <span>Slide 2</span>
      </CarouselElement> */}
    </section>
  );
}

export default MainInfo;
