import { CompanyInfo } from '@elements/CompanyInfo/CompanyInfo';

import styles from './AboutCompany.module.scss';

function AboutCompany() {
  return (
    <section id="company-anchor" className={`${styles.company} bg-secondary`}>
      <div className={`${styles.header} limit-width-wrapper`}>
        <h2 className={`${styles.title} text-dark`}>Наша компания</h2>
        <p className={styles.description}>
          это опыт в реальных и сложных проектах, помноженный на интерес
          сотрудников к достижению новых вершин
        </p>
      </div>

      <CompanyInfo />
    </section>
  );
}

export default AboutCompany;
