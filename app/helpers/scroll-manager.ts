export class ScrollManager {

  public static windowOffset = 0;

  public static setScroll(setBLockState: boolean): void {
    if (setBLockState) {
      this.blockScroll();
    } else {
      this.allowScroll();
    }
  }

  private static blockScroll(): void {
    this.windowOffset = window.scrollY;
    document.body.setAttribute('style', `position: fixed; top: -${this.windowOffset}px; left: 0; right: 0;`);
  }

  private static allowScroll(): void {
    document.body.setAttribute('style', '');
    window.scrollTo(0, this.windowOffset);
  }

}
