/* eslint @typescript-eslint/no-var-requires: "off" */
// @ts-check
const { readFile, writeFile } = require('fs/promises');
const { join } = require('path');

/**
 * Git paths
 */
const HEAD_PATH = join('.git', 'HEAD');
const MESSAGE_PATH = join('.git', 'COMMIT_EDITMSG');

/**
 * Reversed Jira ID pattern
 */
const JIRA_ID_RE = /\d+-[A-Z]+(?!-?[a-zA-Z]{1,10})/g;

/**
 * Reads text file
 * @param {string} p - File path.
 */
const readText = (p) => readFile(p, { encoding: 'utf-8' });

/**
 * Reverses the string
 * @param {string} s
 */
const reverseString = (s) => s
  .split('')
  .reverse()
  .join('');

/**
 * Finds all occurrences of Jira task IDs in a string
 * @param {string} s
 */
function matchJiraIds(s) {
  const matches = reverseString(s).match(JIRA_ID_RE);
  if (matches) {
    return matches.map(reverseString);
  }
  return [];
}

/**
 * Retrieves the name of the current Git branch
 */
const getBranchName = () => readText(HEAD_PATH)
  .then((t) => t.replace('ref: refs/heads/', ''));

/**
 * Retrieves the content of the commit
 */
const getCommitMessage = () => readText(MESSAGE_PATH);

async function main() {
  const [branch, message] = await Promise.all([
    getBranchName(),
    getCommitMessage()
  ]);
  const [branchMatches, messageMatches] = [branch, message].map(matchJiraIds);
  if (branchMatches.length === 1 && messageMatches.length === 0) {
    await writeFile(MESSAGE_PATH, `${branchMatches[0]}: ${message}`);
  }
}

main();

module.exports = {
  matchJiraIds
};
